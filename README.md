# Robôcode MDSC 

### Robô desenvolvido em java para o processo seletivo  **Arena Solutis**

O robô faz um movimento circular na tela, enquanto gira o canhão em 360º, ao avistar  um oponente ele atira e caso seja baleado revida imediatame/nte.

#### Pontos fortes:

* O fato do robô se movimentar em circulo faz com que ele sempre esteja em movimento, sendo um alvo difícil de ser acertado.
* O canhão que gira em 360 º em todo tempo, torna o robô bastante ofensivo, pelo fato dele atirar quando percebe o oponente no raio.

#### Pontos fracos: 

* O  robô está sempre atirando e se desgastando, pois a bala nem sempre atinge o oponente, fazendo com que ele gaste energia desnecessária.
